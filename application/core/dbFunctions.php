<?php

//параметры базы данных   
define("servername", "localhost");
define("password", "root");
define("username", "root");
define("dbname", "GuestBook");

class GuestBook{
    
    private $name;
    private $email;
    private $msg;
    
    public function __construct($_name, $_email, $_msg) {
        $this->name = $_name;
        $this->email = $_email;
        $this->msg = $_msg;
    }
    public function GetName(){
        return $this->name;
    }
    public function GetEmail(){
        return $this->email;
    }
    public function GetMessage(){
        return $this->msg;
    }
    
}

class GuestBookDB extends PDO
{
    public function __construct($servername, $dbname, $username, $password) {
        try{
        parent::__construct("mysql:host=$servername;dbname=$dbname;charset=UTF8", $username, $password);
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    // считываем данные запросом и возвращаем результат ввиде массива экземпляров GuestBook
    // удаляем id, так как он не входит в рамки GuestBook'а
    public function Select(){
        $sqlQuery = "SELECT * FROM Comments";
        $data = $this->prepare($sqlQuery);
        $data->execute();
        $mode = $data->setFetchMode(PDO::FETCH_ASSOC);
        $data = $data->fetchAll();
        $guestBookArray;
        $counter = 0;
        foreach ($data as $key => $value) {
            unset($data[$key]['id']);
            $guestBookArray[$counter] =
                    new GuestBook($data[$key]['name'], $data[$key]['email'], $data[$key]['comment']);
            $counter++;
        }
        return $guestBookArray;
    }
    //шаблон для работы с базой данных, для устранения повторяемости кода
    private function TemplateForInsert($obj){
        $name = $obj->GetName();
        $email = $obj->GetEmail();
        $msg = $obj->GetMessage();
        $sqlQuery = "INSERT INTO Comments (name, email, comment)
                VALUES('$name', '$email', '$msg')";
        $this->exec($sqlQuery);
    }
    // в качестве $obj может прийти как экземпляр GuestBook, так и массив экземпляров GuestBook
    // написал условием, буду благодарен за совет, как сделать поддержку 
    // и массива объектов и просто объекта по другому
    public function Insert($obj){
        if (gettype($obj) == 'array')
        {
            foreach ($obj as $key => $value) {
                $this->TemplateForInsert($value);
            }
        }
        else {
            $this->TemplateForInsert($obj);
        }
    }
    //Максимальная длинна комментария 150 символов, email можно не указывать
    public function CreateTable(){
        $sqlQuery = "CREATE TABLE Comments (
                id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                name VARCHAR(30) NOT NULL,
                email VARCHAR(30),
                comment VARCHAR(150) NOT NULL)
                CHARACTER SET utf8";
        $this->exec($sqlQuery);
    }
    
    // сохраняем пароль в md5
    public function SavePassword($password){

        $encriptedPass = md5($password);
        $sql = "UPDATE ListOfUsers SET password='$encriptedPass' WHERE email='$email'";
        $this->exec($sql);
        
    }
    
}

