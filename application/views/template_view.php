<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="http://localhost:8888/tinyMVC.ru/css/mainpage.css"
    </head>
    <body>
        <div class="heading">
           TGG
        </div>
        
        <nav>
            <ul class='menu'>
                <li><a href="http://localhost:8888/tinyMVC.ru/main">О компании</a></li>
                <li><a href="http://localhost:8888/tinyMVC.ru/contacts">Контактная информация</a></li>
                <li><a href="http://localhost:8888/tinyMVC.ru/guestbook">Гостевая книга</a></li>
            </ul>
        </nav>
        
        <div class="registration">
            <?php
            if ($_SESSION["is_auth"] == false){
                echo 
                "<form name=\"autorization\" method=\"post\" action=\"autorize\">
                <input class=\"field\" type=\"text\" name=\"login\">
                <input class=\"field\" type=\"password\" name=\"password\">
                <input class=\"goto\" type=\"submit\" value=\"Вход\">
                <input class=\"goto\" type=\"button\" value=\"Создать аккаунт\"
                       onClick=\"location.href='http://localhost:8888/tinyMVC.ru/registration'\">
                </form>";
            }
            else{
                echo "<span style=\"font-size:20px; color:white;margin:10px 0px 0px 10px; float:left;\">Здравствуйте, ".$_SESSION["FIO"]."!</span>";
                echo "<input style=\"float:right; margin-right:10px;\" class=\"goto\" type=\"button\" value=\"Выход\""
                . "onClick=\"location.href='http://localhost:8888/tinyMVC.ru/autorize/logout'\">";
            }
            ?>
            
        </div>
        
        <?php include 'application/views/'.$content_view; ?>
    </body>
</html>

