<?php

class Controller_Registration extends Controller

{
    
    public function __construct() {
        
        $this->model = new Model_Registration();
        $this->view = new View();
        
    }
    
    function action_index() {
        $this->view->generate('registration_view.php', 'template_view.php');
        
        
    }
    
    function action_success() {
        if ($_SESSION['is_auth'] == false){
            
            $this->model->Add_New_User();
            $this->view->generate('success_register_view.php', 'template_view.php');
        }
        else
            header('Location: /tinyMVC.ru/main');
        
    }
    public function action_autorize(){
        $this->model->Autorize();
    }
}

