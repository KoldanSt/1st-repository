<?php
class Controller_Guestbook extends Controller
{
    public function __construct() {
        
        $this->model = new Model_Guestbook();
        $this->view = new View();
        
    }
    
    function action_index() {
        
        $data = $this->model->get_data();
        $this->view->generate('guestbook_view.php', 'template_view.php', $data);
        
    }
    public function action_new_comment(){
        $this->view->generate('new_comment_view.php', 'template_view.php');
    }
    public function action_saved_post(){
        
        $this->model->post_results();
        $this->view->generate('saved_post_view.php', 'template_view.php');
    }
    public function action_autorize(){
        $this->model->Autorize();
        $this->view->generate('new_comment_view.php', 'template_view.php');
    }
    
    
}